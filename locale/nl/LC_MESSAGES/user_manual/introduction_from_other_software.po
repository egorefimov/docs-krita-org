# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-03 14:36+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../user_manual/introduction_from_other_software.rst:5
msgid "Introduction Coming From Other Software"
msgstr "Introductie komend uit andere software"

#: ../../user_manual/introduction_from_other_software.rst:7
msgid ""
"Krita is not the only digital painting application in the world. Because we "
"know our users might be approaching Krita with their experience from using "
"other software, we have made guides to illustrate differences."
msgstr ""
"Krita is niet de enige digitale schildertoepassing in de wereld. Omdat we "
"weten dat onze gebruikers Krita kunnen benaderen met hun ervaring uit "
"gebruik van andere software, hebben we gidsen gemaakt om verschillen te "
"illustreren."

#: ../../user_manual/introduction_from_other_software.rst:10
msgid "Contents:"
msgstr "Inhoud:"
