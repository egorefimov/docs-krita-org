# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-06-17 10:53+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../general_concepts/file_formats/file_gbr.rst:1
msgid "The Gimp Brush file format as used in Krita."
msgstr "Het bestandsformaat Gimp Brush zoals gebruikt in Krita."

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "Gimp Brush"
msgstr "Gimp Brush"

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "GBR"
msgstr "GBR"

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "*.gbr"
msgstr "*.gbr"

#: ../../general_concepts/file_formats/file_gbr.rst:15
msgid "\\*.gbr"
msgstr "\\*.gbr"

#: ../../general_concepts/file_formats/file_gbr.rst:17
msgid ""
"The GIMP brush format. Krita can open, save and use these files as :ref:"
"`predefined brushes <predefined_brush_tip>`."
msgstr ""
"Het penseelformaat van GIMP. Krita kan deze bestanden openen, opslaan en "
"gebruiken als :ref:`voorgedefinieerde penselen <predefined_brush_tip>`."

#: ../../general_concepts/file_formats/file_gbr.rst:19
msgid ""
"There's three things that you can decide upon when exporting a ``*.gbr``:"
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:21
msgid "Name"
msgstr "Naam"

#: ../../general_concepts/file_formats/file_gbr.rst:22
msgid ""
"This name is different from the file name, and will be shown inside Krita as "
"the name of the brush."
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:23
msgid "Spacing"
msgstr "Spatiëring"

#: ../../general_concepts/file_formats/file_gbr.rst:24
msgid "This sets the default spacing."
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:26
msgid "Use color as mask"
msgstr "Kleur als masker gebruiken"

#: ../../general_concepts/file_formats/file_gbr.rst:26
msgid ""
"This'll turn the darkest values of the image as the ones that paint, and the "
"whitest as transparent. Untick this if you are using colored images for the "
"brush."
msgstr ""

#: ../../general_concepts/file_formats/file_gbr.rst:28
msgid ""
"GBR brushes are otherwise unremarkable, and limited to 8bit color precision."
msgstr ""
