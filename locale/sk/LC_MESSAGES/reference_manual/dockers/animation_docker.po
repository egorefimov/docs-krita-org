# translation of docs_krita_org_reference_manual___dockers___animation_docker.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___animation_docker\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-04-02 09:35+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/dockers/animation_docker.rst:1
msgid "Overview of the animation docker."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Animation"
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Animation Playback"
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Play/Pause"
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Framerate"
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "FPS"
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Speed"
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:15
msgid "Animation Docker"
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:18
#, fuzzy
#| msgid ".. image:: images/en/Animation_docker.png"
msgid ".. image:: images/dockers/Animation_docker.png"
msgstr ".. image:: images/en/Animation_docker.png"

#: ../../reference_manual/dockers/animation_docker.rst:19
msgid ""
"To have a playback of the animation, you need to use the animation docker."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:21
msgid ""
"The first big box represents the current Frame. The frames are counted with "
"programmer's counting so they start at 0."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:23
msgid ""
"Then there are two boxes for you to change the playback range here. So, if "
"you want to do a 10 frame animation, set the end to 10, and then Krita will "
"cycle through the frames 0 to 10."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:25
msgid ""
"The bar in the middle is filled with playback options, and each of these can "
"also be hot-keyed. The difference between a keyframe and a normal frame in "
"this case is that a normal frame is empty, while a keyframe is filled."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:27
msgid ""
"Then, there's buttons for adding, copying and removing frames. More "
"interesting is the next row:"
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:29
msgid "Onion Skin"
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:30
msgid "Opens the :ref:`onion_skin_docker` if it wasn't open before."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:31
msgid "Auto Frame Mode"
msgstr "Automatický režim rámca"

#: ../../reference_manual/dockers/animation_docker.rst:32
msgid ""
"Will make a frame out of any empty frame you are working on. Currently "
"automatically copies the previous frame."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:34
msgid "Drop frames"
msgstr "Zahodiť snímky"

#: ../../reference_manual/dockers/animation_docker.rst:34
msgid ""
"This'll drop frames if your computer isn't fast enough to show all frames at "
"once. This process is automatic, but the icon will become red if it's forced "
"to do this."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:36
msgid ""
"You can also set the speedup of the playback, which is different from the "
"framerate."
msgstr ""
