# Translation of docs_krita_org_general_concepts.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-03 11:46+0100\n"
"Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts.rst:5
msgid "General Concepts"
msgstr "Conceptes generals"

#: ../../general_concepts.rst:7
msgid ""
"Learn about general art and technology concepts that are not specific to "
"Krita."
msgstr ""
"Apreneu quant als conceptes generals d'art i tecnologia que no són "
"específics del Krita."

#: ../../general_concepts.rst:9
msgid "Contents:"
msgstr "Contingut:"
