# Translation of docs_krita_org_reference_manual___main_menu___image_menu.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 16:06+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../<generated>:1
msgid "Separate Image"
msgstr "Descompon la imatge"

#: ../../reference_manual/main_menu/image_menu.rst:1
msgid "The image menu in Krita."
msgstr "El menú Imatge en el Krita."

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Image"
msgstr "Imatge"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Canvas Projection Color"
msgstr "Color de projecció al llenç"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Trim"
msgstr "Ajusta"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Resize"
msgstr "Redimensiona"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Scale"
msgstr "Escala"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Mirror"
msgstr "Mirall"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Transform"
msgstr "Transforma"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Convert Color Space"
msgstr "Converteix l'espai de color"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Offset"
msgstr "Desplaçament"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Split Channels"
msgstr "Divideix els canals"

#: ../../reference_manual/main_menu/image_menu.rst:16
msgid "Image Menu"
msgstr "El menú Imatge"

#: ../../reference_manual/main_menu/image_menu.rst:18
msgid "Properties"
msgstr "Propietats"

#: ../../reference_manual/main_menu/image_menu.rst:19
msgid "Gives you the image properties."
msgstr "Proporciona les propietats de la imatge."

#: ../../reference_manual/main_menu/image_menu.rst:20
msgid "Image Background Color and Transparency"
msgstr "Color i transparència del fons de la imatge"

#: ../../reference_manual/main_menu/image_menu.rst:21
msgid "Change the background canvas color."
msgstr "Canvia el color de fons del llenç."

#: ../../reference_manual/main_menu/image_menu.rst:22
msgid "Convert Current Image Color Space."
msgstr "Converteix l'espai de color de la imatge actual."

#: ../../reference_manual/main_menu/image_menu.rst:23
msgid "Converts the current image to a new colorspace."
msgstr "Converteix la imatge actual en un espai de color nou."

#: ../../reference_manual/main_menu/image_menu.rst:24
msgid "Trim to image size"
msgstr "Ajusta a la mida de la imatge"

#: ../../reference_manual/main_menu/image_menu.rst:25
msgid ""
"Trims all layers to the image size. Useful for reducing filesize at the loss "
"of information."
msgstr ""
"Ajusta totes les capes a la mida de la imatge. Útil per a reduir la mida del "
"fitxer davant la pèrdua d'informació."

#: ../../reference_manual/main_menu/image_menu.rst:26
msgid "Trim to Current Layer"
msgstr "Ajusta a la capa actual"

#: ../../reference_manual/main_menu/image_menu.rst:27
msgid ""
"A lazy cropping function. Krita will use the size of the current layer to "
"determine where to crop."
msgstr ""
"Una funció d'escapçada posterior. El Krita utilitzarà la mida de la capa "
"actual per a determinar on escapçar."

#: ../../reference_manual/main_menu/image_menu.rst:28
msgid "Trim to Selection"
msgstr "Ajusta a la selecció"

#: ../../reference_manual/main_menu/image_menu.rst:29
msgid ""
"A lazy cropping function. Krita will crop the canvas to the selected area."
msgstr ""
"Una funció d'escapçada posterior. El Krita escapçarà el llenç a l'àrea "
"seleccionada."

#: ../../reference_manual/main_menu/image_menu.rst:30
msgid "Rotate Image"
msgstr "Gira la imatge"

#: ../../reference_manual/main_menu/image_menu.rst:31
msgid "Rotate the image"
msgstr "Gira la imatge."

#: ../../reference_manual/main_menu/image_menu.rst:32
msgid "Shear Image"
msgstr "Inclina la imatge"

#: ../../reference_manual/main_menu/image_menu.rst:33
msgid "Shear the image"
msgstr "Inclina la imatge."

#: ../../reference_manual/main_menu/image_menu.rst:34
msgid "Mirror Image Horizontally"
msgstr "Emmiralla horitzontalment la imatge"

#: ../../reference_manual/main_menu/image_menu.rst:35
msgid "Mirror the image on the horizontal axis."
msgstr "Reflecteix la imatge en l'eix horitzontal."

#: ../../reference_manual/main_menu/image_menu.rst:36
msgid "Mirror Image Vertically"
msgstr "Emmiralla verticalment la imatge"

#: ../../reference_manual/main_menu/image_menu.rst:37
msgid "Mirror the image on the vertical axis."
msgstr "Reflecteix la imatge en l'eix vertical."

#: ../../reference_manual/main_menu/image_menu.rst:38
msgid "Scale to New Size"
msgstr "Escala a una mida nova"

#: ../../reference_manual/main_menu/image_menu.rst:39
msgid ""
"The resize function in any other program with the :kbd:`Ctrl + Alt + I` "
"shortcut."
msgstr ""
"La funció reamida en qualsevol altre programa amb la drecera :kbd:`Ctrl + "
"Alt + I`."

#: ../../reference_manual/main_menu/image_menu.rst:40
msgid "Offset Image"
msgstr "Desplaça la imatge"

#: ../../reference_manual/main_menu/image_menu.rst:41
msgid "Offset all layers."
msgstr "Desplaça totes les capes."

#: ../../reference_manual/main_menu/image_menu.rst:42
msgid "Resize Canvas"
msgstr "Redimensiona el llenç"

#: ../../reference_manual/main_menu/image_menu.rst:43
msgid "Change the canvas size. Don't confuse this with Scale to new size."
msgstr "Canvieu la mida del llenç. No confondre amb Escala a una mida nova."

#: ../../reference_manual/main_menu/image_menu.rst:44
msgid "Image Split"
msgstr "Divideix la imatge"

#: ../../reference_manual/main_menu/image_menu.rst:45
msgid "Calls up the :ref:`image_split` dialog."
msgstr "Crida al diàleg :ref:`image_split`."

#: ../../reference_manual/main_menu/image_menu.rst:46
msgid "Wavelet Decompose"
msgstr "Descompondre per ondetes"

#: ../../reference_manual/main_menu/image_menu.rst:47
msgid "Does :ref:`wavelet_decompose` on the current layer."
msgstr "Fa :ref:`wavelet_decompose` sobre la capa actual."

#: ../../reference_manual/main_menu/image_menu.rst:49
msgid ":ref:`Separates <separate_image>` the image into channels."
msgstr ":ref:`Separa <separate_image>` la imatge en canals."
