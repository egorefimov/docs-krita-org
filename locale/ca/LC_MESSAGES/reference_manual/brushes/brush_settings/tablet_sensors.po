# Translation of docs_krita_org_reference_manual___brushes___brush_settings___tablet_sensors.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 17:05+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../<generated>:1
msgid "Tangential Pressure"
msgstr "Pressió tangencial"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:1
msgid "Tablet sensors in Krita."
msgstr "Sensors de la tauleta en el Krita."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:11
msgid "Tablets"
msgstr "Tauletes"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:16
msgid "Sensors"
msgstr "Sensors"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:18
msgid "Pressure"
msgstr "Pressió"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:19
msgid "Uses the pressure in and out values of your stylus."
msgstr "Utilitza els valors d'entrada i sortida de la pressió del llapis."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:20
msgid "PressureIn"
msgstr "Pressió d'entrada"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:21
msgid ""
"Uses only pressure in values of your stylus. Previous pressure level in same "
"stroke is overwritten *only* by applying more pressure. Lessening the "
"pressure doesn't affect PressureIn."
msgstr ""
"Utilitza només els valors de la pressió d'entrada del llapis. El nivell de "
"pressió anterior en el mateix traç només se sobreescriurà aplicant més "
"pressió. Disminuir la pressió no l'afectarà."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:22
msgid "X-tilt"
msgstr "Inclinació en X"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:23
#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:25
msgid "How much the brush is affected by stylus angle, if supported."
msgstr ""
"Quant es veurà afectat el pinzell per l'angle del llapis, si està admès per "
"la tauleta."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:24
msgid "Y-tilt"
msgstr "Inclinació en Y"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:26
msgid "Tilt-direction"
msgstr "Direcció de la inclinació"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:27
msgid ""
"How much the brush is affected by stylus direction. The pen point pointing "
"towards the user is 0°, and can vary from -180° to +180°."
msgstr ""
"Quant es veurà afectat el pinzell per la direcció del llapis. La punta del "
"llapis que apunta cap a l'usuari és de 0°, i pot variar de -180° a +180°."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:28
msgid "Tilt-elevation"
msgstr "Elevació de la inclinació"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:29
msgid ""
"How much the brush is affected by stylus perpendicularity. 0° is the stylus "
"horizontal, 90° is the stylus vertical."
msgstr ""
"Quant es veurà afectat el pinzell per la perpendicularitat del llapis. 0° és "
"el llapis en horitzontal, 90° és el llapis en vertical."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:30
msgid "Speed"
msgstr "Velocitat"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:31
msgid "How much the brush is affected by the speed at which you draw."
msgstr "Quant es veurà afectat el pinzell per la velocitat a la qual dibuixeu."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:32
msgid "Drawing Angle"
msgstr "Angle de dibuix"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:33
msgid ""
"How much the brush is affected by which direction you are drawing in. :"
"guilabel:`Lock` will lock the angle to the one you started the stroke with. :"
"guilabel:`Fan corners` will try to smoothly round the corners, with the "
"angle being the angles threshold it'll round. :guilabel:`Angle offset` will "
"add an extra offset to the current angle."
msgstr ""
"Quant es veurà afectat el pinzell per la direcció en la que esteu "
"dibuixant. :guilabel:`Bloqueja`, bloquejarà l'angle amb el qual vàreu "
"començar el traç. :guilabel:`Cantonades arrodonides`, intentarà arrodonir "
"suaument les cantonades, amb l'angle sent el llindar dels angles que "
"s'arrodoniran. :guilabel:`Desplaçament de l'angle` afegirà un desplaçament "
"addicional al de l'angle actual."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:34
msgid "Rotation"
msgstr "Gir"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:35
msgid ""
"How much a brush is affected by how the stylus is rotated, if supported by "
"the tablet."
msgstr ""
"Quant es veurà afectat el pinzell per com gira el llapis, si està admès per "
"la tauleta."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:36
msgid "Distance"
msgstr "Distància"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:37
msgid "How much the brush is affected over length in pixels."
msgstr "Quant es veurà afectat el pinzell sobre la longitud en píxels."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:38
msgid "Time"
msgstr "Temps"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:39
msgid "How much a brush is affected over drawing time in seconds."
msgstr "Quant es veurà afectat el pinzell sobre el temps de dibuix en segons."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:40
msgid "Fuzzy (Dab)"
msgstr "Toc difús"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:41
msgid "Basically the random option."
msgstr "Bàsicament l'opció aleatòria."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:42
msgid "Fuzzy Stroke"
msgstr "Traç difús"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:43
msgid ""
"A randomness value that is per stroke. Useful for getting color and size "
"variation in on speed-paint brushes."
msgstr ""
"Un valor d'aleatorietat que és pel traç. Útil per obtenir la variació de "
"color i mida en els pinzells de pintura ràpida."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:44
msgid "Fade"
msgstr "Esvaeix"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:45
msgid ""
"How much the brush is affected over length, proportional to the brush size."
msgstr ""
"Quant es veurà afectat el pinzell sobre la longitud, proporcional amb la "
"mida del pinzell."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:46
msgid "Perspective"
msgstr "Perspectiva"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:47
msgid "How much the brush is affected by the perspective assistant."
msgstr "Quant es veurà afectat el pinzell per l'assistent de la perspectiva."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:49
msgid ""
"How much the brush is affected by the wheel on airbrush-simulating styli."
msgstr ""
"Quant es veurà afectat el pinzell per la roda sobre l'estil simulant un "
"aerògraf."
