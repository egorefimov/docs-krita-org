# Translation of docs_krita_org_reference_manual___tools___color_selector.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 16:09+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/tools/color_selector.rst:1
msgid "Krita's color selector tool reference."
msgstr "Referència de l'eina Selector de color del Krita."

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Colors"
msgstr "Colors"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Eyedropper"
msgstr "Comptagotes"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Color Selector"
msgstr "Selector de color"

#: ../../reference_manual/tools/color_selector.rst:17
msgid "Color Selector Tool"
msgstr "Eina del selector de color"

#: ../../reference_manual/tools/color_selector.rst:20
msgid ""
"This tool allows you to choose a point from the canvas and make the color of "
"that point the active foreground color. When a painting or drawing tool is "
"selected the Color Picker tool can also be quickly accessed by pressing the :"
"kbd:`Ctrl` key."
msgstr ""
"Aquesta eina permet triar un punt del llenç i fer que el color d'aquest punt "
"sigui el color de primer pla actiu. Quan se selecciona una eina de pintura o "
"dibuix, també es podrà accedir ràpidament a l'eina Selector de color prement "
"la tecla :kbd:`Ctrl`."

#: ../../reference_manual/tools/color_selector.rst:23
msgid ".. image:: images/tools/Color_Dropper_Tool_Options.png"
msgstr ".. image:: images/tools/Color_Dropper_Tool_Options.png"

#: ../../reference_manual/tools/color_selector.rst:24
msgid ""
"There are several options shown in the :guilabel:`Tool Options` docker when "
"the :guilabel:`Color Picker` tool is active:"
msgstr ""
"Hi ha diverses opcions que es mostren a l'acoblador :guilabel:`Opcions de "
"l'eina` quan està activa l'eina :guilabel:`Selector de color`:"

#: ../../reference_manual/tools/color_selector.rst:26
msgid ""
"The first drop-down box allows you to select whether you want to sample from "
"all visible layers or only the active layer. You can choose to have your "
"selection update the current foreground color, to be added into a color "
"palette, or to do both."
msgstr ""
"La primera llista desplegable permet seleccionar si voleu mostrejar des de "
"totes les capes visibles o només des de la capa activa. També podreu triar "
"que la vostra selecció actualitzi el color de primer pla actual, el qual "
"s'afegirà a una paleta de colors, o que faci ambdues coses."

#: ../../reference_manual/tools/color_selector.rst:30
msgid ""
"The middle section contains a few properties that change how the Color "
"Picker picks up color; you can set a :guilabel:`Radius`, which will average "
"the colors in the area around the cursor, and you can now also set a :"
"guilabel:`Blend` percentage, which controls how much color is \"soaked up\" "
"and mixed in with your current color. Read :ref:`mixing_colors` for "
"information about how the Color Picker's blend option can be used as a tool "
"for off-canvas color mixing."
msgstr ""
"La secció central conté unes quantes propietats que canvien la forma en què "
"el Selector de color pren el color. Podreu establir un :guilabel:`Radi`, el "
"qual treurà la mitjana dels colors en l'àrea al voltant del cursor, i ara "
"també podreu establir un percentatge de :guilabel:`Barreja`, el qual "
"controlarà la quantitat de color que s'«absorbirà» i es mesclarà amb el "
"vostre color actual. Llegiu :ref:`mixing_colors` per obtenir informació "
"sobre com es pot utilitzar l'opció de barreja del Selector de color com una "
"eina per a mesclar els colors que es troben fora del llenç."

#: ../../reference_manual/tools/color_selector.rst:32
msgid ""
"At the very bottom is the Info Box, which displays per-channel data about "
"your most recently picked color. Color data can be shown as 8-bit numbers or "
"percentages."
msgstr ""
"A la part inferior es troba el Rètol informatiu, el qual mostrarà les dades "
"per canal sobre el color seleccionat més recentment. Les dades de color es "
"poden mostrar com a números de 8 bits o percentatges."
