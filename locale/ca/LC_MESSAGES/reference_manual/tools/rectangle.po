# Translation of docs_krita_org_reference_manual___tools___rectangle.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 16:05+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Ratio"
msgstr "Relació"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:26
msgid ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: toolrectangle"
msgstr ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: eina de rectangle"

#: ../../reference_manual/tools/rectangle.rst:1
msgid "Krita's rectangle tool reference."
msgstr "Referència de l'eina Rectangle del Krita."

#: ../../reference_manual/tools/rectangle.rst:10
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/rectangle.rst:10
msgid "Rectangle"
msgstr "Rectangle"

#: ../../reference_manual/tools/rectangle.rst:15
msgid "Rectangle Tool"
msgstr "Eina de rectangle"

#: ../../reference_manual/tools/rectangle.rst:17
msgid "|toolrectangle|"
msgstr "|toolrectangle|"

#: ../../reference_manual/tools/rectangle.rst:19
msgid ""
"This tool can be used to paint rectangles, or create rectangle shapes on a "
"vector layer. Click and hold |mouseleft| to indicate one corner of the "
"rectangle, drag to the opposite corner, and release the button."
msgstr ""
"Aquesta eina es pot utilitzar per a pintar rectangles o crear formes de "
"rectangle sobre una capa vectorial. Feu |mouseleft| i manteniu-lo premut per "
"indicar una cantonada del rectangle, arrossegueu cap a la cantonada oposada "
"i deixeu anar el botó."

#: ../../reference_manual/tools/rectangle.rst:22
msgid "Hotkeys and Sticky-keys"
msgstr "Dreceres i tecles apegaloses"

#: ../../reference_manual/tools/rectangle.rst:24
msgid "There's no default hotkey for switching to rectangle."
msgstr "No hi ha cap drecera predeterminada per a canviar a rectangle."

#: ../../reference_manual/tools/rectangle.rst:26
msgid ""
"If you hold the :kbd:`Shift` key while drawing, a square will be drawn "
"instead of a rectangle. Holding the :kbd:`Ctrl` key will change the way the "
"rectangle is constructed. Normally, the first mouse click indicates one "
"corner and the second click the opposite. With the :kbd:`Ctrl` key, the "
"initial mouse position indicates the center of the rectangle, and the final "
"mouse position indicates a corner. You can press the :kbd:`Alt` key while "
"still keeping |mouseleft| down to move the rectangle to a different location."
msgstr ""
"Si manteniu premuda la tecla :kbd:`Majús.` mentre dibuixeu, es dibuixarà un "
"quadrat en lloc d'un rectangle. Mantenint premuda la tecla :kbd:`Ctrl` es "
"canviarà la manera en què es construirà el rectangle. Normalment, el primer "
"clic del ratolí indicarà una cantonada i el segon l'oposada. Amb la tecla :"
"kbd:`Ctrl`, la posició inicial del ratolí indicarà el centre del rectangle, "
"i la posició final del ratolí indicarà una cantonada. Podeu prémer la tecla :"
"kbd:`Alt` mentre manteniu premut el |mouseleft| per a moure el rectangle a "
"una ubicació diferent."

#: ../../reference_manual/tools/rectangle.rst:28
msgid ""
"You can change between the corner/corner and center/corner drawing methods "
"as often as you want by pressing or releasing the :kbd:`Ctrl` key, provided "
"that you keep |mouseleft| pressed. With the :kbd:`Ctrl` key pressed, mouse "
"movements will affect all four corners of the rectangle (relative to the "
"center), without the :kbd:`Ctrl` key, one of the corners is unaffected."
msgstr ""
"Podreu canviar entre els mètodes de dibuix cantonada/cantonada i centre/"
"cantonada tan sovint com vulgueu prement o deixant anar la tecla :kbd:"
"`Ctrl`, sempre que es mantingui premuda. Prement la tecla :kbd:`Ctrl`, els "
"moviments del ratolí afectaran a les quatre cantonades del rectangle (en "
"relació amb el centre), sense la tecla :kbd:`Ctrl`, una de les cantonades no "
"es veurà afectada."

#: ../../reference_manual/tools/rectangle.rst:32
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/rectangle.rst:35
msgid "Fill"
msgstr "Emplenat"

#: ../../reference_manual/tools/rectangle.rst:37
msgid "Not filled"
msgstr "Sense emplenar"

#: ../../reference_manual/tools/rectangle.rst:38
msgid "The rectangle will be transparent from the inside."
msgstr "El rectangle serà transparent des de l'interior."

#: ../../reference_manual/tools/rectangle.rst:39
msgid "Foreground color"
msgstr "Color de primer pla"

#: ../../reference_manual/tools/rectangle.rst:40
msgid "The rectangle will use the foreground color as fill."
msgstr "El rectangle utilitzarà el color de primer pla com a emplenat."

#: ../../reference_manual/tools/rectangle.rst:41
msgid "Background color"
msgstr "Color de fons"

#: ../../reference_manual/tools/rectangle.rst:42
msgid "The rectangle will use the background color as fill."
msgstr "El rectangle utilitzarà el color de fons com a emplenat."

#: ../../reference_manual/tools/rectangle.rst:44
msgid "Pattern"
msgstr "Patró"

#: ../../reference_manual/tools/rectangle.rst:44
msgid "The rectangle will use the active pattern as fill."
msgstr "El rectangle utilitzarà el patró actiu com a emplenat."

#: ../../reference_manual/tools/rectangle.rst:47
msgid "Outline"
msgstr "Contorn"

#: ../../reference_manual/tools/rectangle.rst:49
msgid "No Outline"
msgstr "Sense contorn"

#: ../../reference_manual/tools/rectangle.rst:50
msgid "The Rectangle will render without outline."
msgstr "El rectangle es representarà sense contorn."

#: ../../reference_manual/tools/rectangle.rst:52
msgid "Brush"
msgstr "Pinzell"

#: ../../reference_manual/tools/rectangle.rst:52
msgid "The Rectangle will use the current selected brush to outline."
msgstr "El rectangle utilitzarà el pinzell seleccionat per al contorn."

#: ../../reference_manual/tools/rectangle.rst:55
msgid ""
"On vector layers, the rectangle will not render with a brush outline, but "
"rather a vector outline."
msgstr ""
"Sobre les capes vectorials, el rectangle no es representarà amb un contorn "
"del pinzell, sinó amb un contorn vectorial."

#: ../../reference_manual/tools/rectangle.rst:57
msgid "Anti-aliasing"
msgstr "Antialiàsing"

#: ../../reference_manual/tools/rectangle.rst:58
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Alterna entre donar o no seleccions amb vores suaus. Hi ha qui prefereix "
"vores precises per a les seves seleccions."

#: ../../reference_manual/tools/rectangle.rst:59
msgid "Width"
msgstr "Amplada"

#: ../../reference_manual/tools/rectangle.rst:60
msgid ""
"Gives the current width. Use the lock to force the next selection made to "
"this width."
msgstr ""
"Dóna l'amplada actual. Utilitzeu el bloqueig per a forçar que la següent "
"selecció es realitzi amb aquesta amplada."

#: ../../reference_manual/tools/rectangle.rst:61
msgid "Height"
msgstr "Alçada"

#: ../../reference_manual/tools/rectangle.rst:62
msgid ""
"Gives the current height. Use the lock to force the next selection made to "
"this height."
msgstr ""
"Dóna l'alçada actual. Utilitzeu el bloqueig per a forçar que la següent "
"selecció es realitzi amb aquesta alçada."

#: ../../reference_manual/tools/rectangle.rst:66
msgid ""
"Gives the current ratio. Use the lock to force the next selection made to "
"this ratio."
msgstr ""
"Dóna la proporció actual. Utilitzeu el bloqueig per a forçar que la següent "
"selecció es realitzi amb aquesta proporció."

#: ../../reference_manual/tools/rectangle.rst:68
msgid "Round X"
msgstr "Arrodoniment de X"

#: ../../reference_manual/tools/rectangle.rst:70
msgid "The horizontal radius of the rectangle corners."
msgstr "El radi horitzontal de les cantonades del rectangle."

#: ../../reference_manual/tools/rectangle.rst:72
msgid "Round Y"
msgstr "Arrodoniment de Y"

#: ../../reference_manual/tools/rectangle.rst:74
msgid "The vertical radius of the rectangle corners."
msgstr "El radi vertical de les cantonades del rectangle."
