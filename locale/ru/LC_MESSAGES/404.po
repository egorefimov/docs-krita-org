# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-24 02:56+0300\n"
"Last-Translator: Alexander Potashev <aspotashev@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../404.rst:None
msgid ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Image of Kiki looking confused through books."
msgstr ""

#: ../../404.rst:5
msgid "File Not Found (404)"
msgstr "Страница не найдена (404)"

#: ../../404.rst:10
msgid "This page does not exist."
msgstr ""

#: ../../404.rst:12
msgid "This might be because of the following:"
msgstr ""

#: ../../404.rst:14
msgid ""
"We moved the manual from MediaWiki to Sphinx and with that came a lot of "
"reorganization."
msgstr ""

#: ../../404.rst:15
msgid "The page has been deprecated."
msgstr ""

#: ../../404.rst:16
msgid "Or a simple typo in the url."
msgstr ""

#: ../../404.rst:18
msgid ""
"In all cases, you might be able to find the page you're looking for by using "
"the search in the lefthand navigation."
msgstr ""
