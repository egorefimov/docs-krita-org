# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-30 20:37+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/file_formats/file_kra.rst:1
msgid "The Krita Raster Archive file format."
msgstr "Kritas rastreringsarkivfilformat."

#: ../../general_concepts/file_formats/file_kra.rst:10
msgid "*.kra"
msgstr "*.kra"

#: ../../general_concepts/file_formats/file_kra.rst:10
msgid "KRA"
msgstr "KRA"

#: ../../general_concepts/file_formats/file_kra.rst:10
msgid "Krita Archive"
msgstr "Krita-arkiv"

#: ../../general_concepts/file_formats/file_kra.rst:15
msgid "\\*.kra"
msgstr "\\*.kra"

#: ../../general_concepts/file_formats/file_kra.rst:17
msgid ""
"``.kra`` is Krita's internal file-format, which means that it is the file "
"format that saves all of the features Krita can handle. It's construction is "
"vaguely based on the open document standard, which means that you can rename "
"your ``.kra`` file to a ``.zip`` file and open it up to look at the insides."
msgstr ""
"Kritas interna filformat är ``.kra``, vilket betyder att det är filformatet "
"som sparar all funktionalitet som Krita kan hantera. Dess konstruktion är "
"grovt sett baserad på den öppna dokumentstandarden, vilket betyder att du "
"kan byta namn på en ``.kra``-fil till en ``.zip``-fil och öppna det för att "
"titta vad som finns inuti."

#: ../../general_concepts/file_formats/file_kra.rst:19
msgid ""
"It is a format that you can expect to get very heavy, and isn't meant for "
"sharing on the internet."
msgstr ""
"Det är ett format som kan förväntas vara mycket omfattande, och är inte "
"avsett för att dela med sig på Internet."
