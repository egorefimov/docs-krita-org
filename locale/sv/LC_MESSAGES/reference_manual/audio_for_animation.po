# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-30 09:52+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/audio_for_animation.rst:1
msgid "The audio playback with animation in Krita."
msgstr "Ljuduppspelning med animering i Krita."

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Animation"
msgstr "Animering"

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Audio"
msgstr "Audio"

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Sound"
msgstr "Ljud"

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Timeline"
msgstr "Tidslinje"

#: ../../reference_manual/audio_for_animation.rst:17
msgid "Audio for Animation"
msgstr "Ljud för animering"

#: ../../reference_manual/audio_for_animation.rst:21
msgid ""
"Audio for animation is an unfinished feature. It has multiple bugs and may "
"not work on your system."
msgstr ""
"Ljud i animeringar är en funktion som inte är färdig. Den har flera fel och "
"kanske inte fungerar på din dator."

#: ../../reference_manual/audio_for_animation.rst:23
msgid ""
"You can add audio files to your animation to help sync lips or music. This "
"functionality is available in the timeline docker."
msgstr ""
"Det går att lägga till ljudfiler i animeringar för att hjälpa till att "
"synkronisera läpprörelser eller musik. Funktionen är tillgänglig i "
"tidslinjepanelen."

#: ../../reference_manual/audio_for_animation.rst:26
msgid "Importing Audio Files"
msgstr "Importera ljudfiler"

#: ../../reference_manual/audio_for_animation.rst:28
msgid ""
"Krita supports MP3, OGM, and WAV audio files. When you open up your timeline "
"docker, there will be a speaker button in the top left area."
msgstr ""
"Krita stöder MP3-, OGM- och WAV-ljudfiler. När man öppnar tidslinjepanelen, "
"finns det en högtalarknapp i området längst upp till vänster."

#: ../../reference_manual/audio_for_animation.rst:30
msgid ""
"If you press the speaker button, you will get the available audio options "
"for the animation."
msgstr ""
"Om man klickar på högtalarknappen ser man tillgängliga ljudalternativ för "
"animeringen."

#: ../../reference_manual/audio_for_animation.rst:32
msgid "Open"
msgstr "Öppna"

#: ../../reference_manual/audio_for_animation.rst:33
msgid "Mute"
msgstr "Tyst"

#: ../../reference_manual/audio_for_animation.rst:34
msgid "Remove audio"
msgstr "Ta bort ljud"

#: ../../reference_manual/audio_for_animation.rst:35
msgid "Volume slider"
msgstr "Volymreglage"

#: ../../reference_manual/audio_for_animation.rst:37
msgid ""
"Krita saves the location of your audio file. If you move the audio file or "
"rename it, Krita will not be able to find it. Krita will tell you the file "
"was moved or deleted the next time you try to open the Krita file up."
msgstr ""
"Krita sparar ljudfilens plats. Om ljudfilen flyttas eller namnet ändras, kan "
"Krita inte hitta den. Krita talar om att filen har flyttats eller tagits "
"bort nästa gång man försöker öppna filen i Krita."

#: ../../reference_manual/audio_for_animation.rst:40
msgid "Using Audio"
msgstr "Använda ljud"

#: ../../reference_manual/audio_for_animation.rst:42
msgid ""
"After you import the audio, you can scrub through the timeline and it will "
"play the audio chunk at the time spot. When you press the Play button, the "
"entire the audio file will playback as it will in the final version. There "
"is no visual display of the audio file on the screen, so you will need to "
"use your ears and the scrubbing functionality to position frames."
msgstr ""
"Efter att ha importerat ljud, kan man spola igenom tidslinjen och "
"ljudstycket spelas upp vid tidpunkten.När man klickar på uppspelningsknappen "
"spelas hela ljudfilen som den kommer att göras i slutversionen. Det finns "
"ingen synlig visning av ljudfilen på skärmen, så man måste använda hörseln "
"och spolningsfunktionen för att positionera rutorna."

#: ../../reference_manual/audio_for_animation.rst:46
msgid "Exporting with Audio"
msgstr "Exportera med ljud"

#: ../../reference_manual/audio_for_animation.rst:48
msgid ""
"To get the audio file included when you are exporting, you need to include "
"it in the Render Animation options. In the :menuselection:`File --> Render "
"Animation` options there is a checkbox :guilabel:`Include Audio`. Make sure "
"that is checked before you export and you should be good to go."
msgstr ""
"För att få med ljudfilen när man exporterar, måste den inkluderas i "
"alternativen för att återge animeringen. Under alternativen i :menuselection:"
"`Arkiv --> Återge animering` finns kryssrutan :guilabel:`Inkludera ljud`. "
"Säkerställ att den är markerad före export, så är du redo att fortsätta."

#: ../../reference_manual/audio_for_animation.rst:51
msgid "Packages needed for Audio on Linux"
msgstr "Paket som krävs för ljud på Linux"

#: ../../reference_manual/audio_for_animation.rst:53
msgid ""
"The following packages are necessary for having the audio support on Linux:"
msgstr "Följande paket är nödvändiga för att få stöd för ljud på Linux:"

#: ../../reference_manual/audio_for_animation.rst:56
msgid "For people who build Krita on Linux:"
msgstr "För de som bygger Krita på Linux:"

#: ../../reference_manual/audio_for_animation.rst:58
msgid "libasound2-dev"
msgstr "libasound2-dev"

#: ../../reference_manual/audio_for_animation.rst:59
msgid "libgstreamer1.0-dev gstreamer1.0-pulseaudio"
msgstr "libgstreamer1.0-dev gstreamer1.0-pulseaudio"

#: ../../reference_manual/audio_for_animation.rst:60
msgid "libgstreamer-plugins-base1.0-dev"
msgstr "libgstreamer-plugins-base1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:61
msgid "libgstreamer-plugins-good1.0-dev"
msgstr "libgstreamer-plugins-good1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:62
msgid "libgstreamer-plugins-bad1.0-dev"
msgstr "libgstreamer-plugins-bad1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:64
msgid "For people who use Krita on Linux:"
msgstr "För de som använder Krita på Linux:"

#: ../../reference_manual/audio_for_animation.rst:66
msgid "libqt5multimedia5-plugins"
msgstr "libqt5multimedia5-plugins"

#: ../../reference_manual/audio_for_animation.rst:67
msgid "libgstreamer-plugins-base1.0"
msgstr "libgstreamer-plugins-base1.0"

#: ../../reference_manual/audio_for_animation.rst:68
msgid "libgstreamer-plugins-good1.0"
msgstr "libgstreamer-plugins-good1.0"

#: ../../reference_manual/audio_for_animation.rst:69
msgid "libgstreamer-plugins-bad1.0"
msgstr "libgstreamer-plugins-bad1.0"
