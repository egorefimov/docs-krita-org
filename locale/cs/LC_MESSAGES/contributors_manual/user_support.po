# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Vit Pelcak <vit@pelcak.org>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-09 03:10+0200\n"
"PO-Revision-Date: 2019-08-29 16:39+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 19.08.0\n"

#: ../../contributors_manual/user_support.rst:1
msgid "Introduction to user support."
msgstr ""

#: ../../contributors_manual/user_support.rst:20
msgid "Introduction to User Support"
msgstr ""

#: ../../contributors_manual/user_support.rst:35
msgid "Contents"
msgstr "Obsah"

#: ../../contributors_manual/user_support.rst:38
msgid "Tablet Support"
msgstr ""

#: ../../contributors_manual/user_support.rst:40
msgid ""
"The majority of help requests are about pen pressure and tablet support in "
"general."
msgstr ""

#: ../../contributors_manual/user_support.rst:44
msgid "Quick solutions"
msgstr ""

#: ../../contributors_manual/user_support.rst:46
msgid ""
"On Windows: reinstall your driver (Windows Update often breaks tablet driver "
"settings, reinstallation helps)."
msgstr ""

#: ../../contributors_manual/user_support.rst:48
msgid ""
"Change API in :menuselection:`Settings --> Configure Krita --> Tablet "
"Settings` (for some devices, especially N-trig ones, Windows Ink work "
"better, for some it's Wintab)."
msgstr ""

#: ../../contributors_manual/user_support.rst:50
msgid ""
"On Windows, Wacom tablets: if you get straight lines at the beginnings of "
"the strokes, disable/minimize \"double-click distance\" in Wacom settings."
msgstr ""

#: ../../contributors_manual/user_support.rst:53
msgid "Gathering information"
msgstr ""

#: ../../contributors_manual/user_support.rst:55
msgid "Which OS do you use?"
msgstr ""

#: ../../contributors_manual/user_support.rst:57
msgid "Which tablet do you have?"
msgstr ""

#: ../../contributors_manual/user_support.rst:59
msgid "What is the version of the tablet driver?"
msgstr ""

#: ../../contributors_manual/user_support.rst:61
msgid ""
"Please collect Tablet Tester (:menuselection:`Settings --> Configure Krita --"
"> Tablet Settings`) output, paste it to `Pastebin <https://pastebin.com/>`_ "
"or similar website and give us a link."
msgstr ""

#: ../../contributors_manual/user_support.rst:65
msgid "Additional information for supporters"
msgstr ""

#: ../../contributors_manual/user_support.rst:67
msgid ""
"Except for the issue with beginnings of the strokes, Wacom tablets usually "
"work no matter the OS."
msgstr ""

#: ../../contributors_manual/user_support.rst:69
msgid ""
"Huion tablets should work on Windows and on Linux, on Mac there might be "
"issues."
msgstr ""

#: ../../contributors_manual/user_support.rst:71
msgid "XP-Pen tablets and other brands can have issues everywhere."
msgstr ""

#: ../../contributors_manual/user_support.rst:73
msgid ""
"If someone asks about a tablet to buy, generally a cheaper Wacom or a Huion "
"are the best options as of 2019, if they want to work with Krita. :ref:"
"`list_supported_tablets`"
msgstr ""

#: ../../contributors_manual/user_support.rst:75
msgid ""
"`Possibly useful instruction in case of XP-Pen tablet issues <https://www."
"reddit.com/r/krita/comments/btzh72/"
"xppen_artist_12s_issue_with_krita_how_to_fix_it/>`_."
msgstr ""

#: ../../contributors_manual/user_support.rst:79
msgid "Animation"
msgstr "Animace"

#: ../../contributors_manual/user_support.rst:81
msgid ""
"Issues with rendering animation can be of various shapes and colors. First "
"thing to find out is whether the issue happens on Krita's or FFmpeg's side "
"(Krita saves all the frames, then FFmpeg is used to render a video using "
"this sequence of images). To learn that, instruct the user to render as "
"\"Image Sequence\". If the image sequence is correct, FFmpeg (or more often: "
"render options) are at fault. If the image sequence is incorrect, either the "
"options are wrong (if for example not every frame got rendered), or it's a "
"bug in Krita."
msgstr ""

#: ../../contributors_manual/user_support.rst:85
msgid ""
"If the user opens the Log Viewer docker, turns on logging and then tries to "
"render a video, Krita will print out the whole ffmpeg command to Log Viewer "
"so it can be easily investigated."
msgstr ""

#: ../../contributors_manual/user_support.rst:87
msgid ""
"There is a log file in the directory that user tries to render to. It can "
"contain information useful to investigation of the issue."
msgstr ""

#: ../../contributors_manual/user_support.rst:90
msgid "Onion skin issues"
msgstr ""

#: ../../contributors_manual/user_support.rst:92
msgid ""
"The great majority of issues with onion skin are just user errors, not bugs. "
"Nonetheless, you need to find out why it happens and direct the user how to "
"use onion skin properly."
msgstr ""

#: ../../contributors_manual/user_support.rst:96
msgid "Crash"
msgstr ""

#: ../../contributors_manual/user_support.rst:98
msgid ""
"In case of crash try to determine if the problem is known, if not, instruct "
"user to create a bug report (or create it yourself) with following "
"information:"
msgstr ""

#: ../../contributors_manual/user_support.rst:100
msgid "What happened, what was being done just before the crash."
msgstr ""

#: ../../contributors_manual/user_support.rst:102
msgid ""
"Is it possible to reproduce (repeat)? If yes, provide a step-by-step "
"instruction to get the crash."
msgstr ""

#: ../../contributors_manual/user_support.rst:104
msgid ""
"Backtrace (crashlog) -- the instruction is here: :ref:`dr_minw`, and the "
"debug symbols can be found in the annoucement of the version of Krita that "
"the user has. But it could be easier to just point the user to `https://"
"download.kde.org/stable/krita <https://download.kde.org/stable/krita>`_."
msgstr ""

#: ../../contributors_manual/user_support.rst:108
msgid "Other possible questions with quick solutions"
msgstr ""

#: ../../contributors_manual/user_support.rst:110
msgid ""
"When the user has trouble with anything related to preview or display, ask "
"them to change :guilabel:`Canvas Graphics Acceleration` in :menuselection:"
"`Settings --> Configure Krita --> Display`."
msgstr ""

#: ../../contributors_manual/user_support.rst:116
msgid ""
"When the user has any weird issue, something you've never heard about, ask "
"them to reset the configuration: :ref:`faq_reset_krita_configuration`."
msgstr ""

#: ../../contributors_manual/user_support.rst:120
msgid "Advices for supporters"
msgstr ""

#: ../../contributors_manual/user_support.rst:122
msgid ""
"If you don't understand the question, ask for clarification -- asking for a "
"screen recording or a screenshot is perfectly fine."
msgstr ""

#: ../../contributors_manual/user_support.rst:124
msgid ""
"If you don't know the solution but you know what information will be needed "
"to investigate the issue further, don't hesitate to ask. Other supporters "
"may know the answer, but have too little time to move the user through the "
"whole process, so you're helping a lot just by asking for additional "
"information. This is very much true in case of tablet issues, for example."
msgstr ""

#: ../../contributors_manual/user_support.rst:126
msgid ""
"If you don't know the answer/solution and the question looks abandoned by "
"other supporters, you can always ask for help on Krita IRC channel. It's "
"#krita on freenote.net: :ref:`the_krita_community`."
msgstr ""

#: ../../contributors_manual/user_support.rst:128
msgid ""
"Explain steps the user needs to make clearly, for example if you need them "
"to change something in settings, clearly state the whole path of buttons and "
"tabs to get there."
msgstr ""

#: ../../contributors_manual/user_support.rst:130
msgid ""
"Instead of :menuselection:`Settings --> Configure Krita` use just :"
"menuselection:`Configure Krita` -- it's easy enough to find and Mac users "
"(where you need to select :menuselection:`Krita --> Settings`) won't get "
"confused."
msgstr ""

#: ../../contributors_manual/user_support.rst:132
msgid ""
"If you ask for an image, mention usage of `Imgur <https://imgur.com>`_ or "
"`Pasteboard <https://pasteboard.co>`_, otherwise Reddit users might create a "
"new post with this image instead of including it to the old conversation."
msgstr ""

#: ../../contributors_manual/user_support.rst:134
msgid ""
"If you want to quickly answer someone, just link to the appropriate place in "
"this manual page -- you can click on the little link icon next to the "
"section or subsection title and give the link to the user so they for "
"example know what information about their tablet issue you need."
msgstr ""

#: ../../contributors_manual/user_support.rst:136
msgid ""
"If the user access the internet from the country or a workplace with some of "
"the websites blocked (like imgur.com or pastebin.com), here is a list of "
"alternatives that works:"
msgstr ""

#: ../../contributors_manual/user_support.rst:138
msgid "Images (e.g. screenshots): `Pasteboard <https://pasteboard.co>`_"
msgstr ""

#: ../../contributors_manual/user_support.rst:140
msgid ""
"Text only: `BPaste <https://bpaste.net>`_, `paste.ubuntu.org.cn <paste."
"ubuntu.org.cn>`_, `paste.fedoraproject.org <https://paste.fedoraproject.org/"
">`_ or `https://invent.kde.org/dashboard/snippets (needs KDE Identity) "
"<https://invent.kde.org/dashboard/snippets>`_."
msgstr ""

#: ../../contributors_manual/user_support.rst:142
msgid ""
"``.kra`` and other formats: by mail? Or encode the file using `base64` "
"command on Linux, send by mail or on Pastebin, then decode using the same "
"command."
msgstr ""

#: ../../contributors_manual/user_support.rst:147
msgid ""
"If you ask user to store their log or other data on a website, make sure it "
"stays there long enough for you to get it -- for example bpaste.net stores "
"files by default only for a day! And you can extend it only to one week."
msgstr ""

#: ../../contributors_manual/user_support.rst:149
msgid ""
"Make sure they don't post their personal data. Tablet Tester log is safe, "
"log from the :menuselection:`Help -> Show system information for bug "
"reports` might not be that safe. Maybe you could ask them to send it to you "
"by mail?"
msgstr ""
