msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___introduction_from_other_software___introduction_from_sai."
"pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: 鼠标左键"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: 鼠标右键"

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: 鼠标中间键"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/filters/Krita-color-to-alpha.png"
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/dockers/Krita_Color_Selector_Types.png"
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/tools/Krita-multibrush.png"
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/Krita-view-dependant-lut-management.png"
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/tools/Krita_transforms_recursive.png"
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/Krita_ghostlady_3.png"
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:None
msgid ".. image:: images/Krita-popuppalette.png"
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:1
msgid "This is a introduction to Krita for users coming from Paint Tool Sai."
msgstr "面向从 Paint Tool SAI 迁移到 Krita 的用户的上手指南。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:12
msgid "Sai"
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:12
msgid "Painttool Sai"
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:17
msgid "Introduction to Krita coming from Paint Tool Sai"
msgstr "Paint Tool SAI 用户入门 Krita"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:20
msgid "How do you do that in Krita?"
msgstr "SAI 在 Krita 中的对应操作"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:22
msgid ""
"This section goes over the functionalities that Krita and Paint Tool Sai "
"share, but shows how they slightly differ."
msgstr ""
"本小节将介绍 Krita 和 Paint Tool SAI 的共通功能，并指出它们之间的微小差异。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:25
msgid "Canvas navigation"
msgstr "画布视图控制"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:27
msgid ""
"Krita, just like Sai, allows you to flip, rotate and duplicate the view. "
"Unlike Sai, these are tied to keyboard keys."
msgstr ""
"Krita 和 SAI 一样可以镜像、旋转和缩放画布视图。但和 SAI 不同的是这些功能被指"
"定到了键盘快捷键。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:29
msgid "Mirror"
msgstr "镜像"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:30
msgid "This is tied to :kbd:`M` key to flip."
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:31
msgid "Rotate"
msgstr "旋转"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:32
msgid ""
"There's a couple of possibilities here: either the :kbd:`4` and :kbd:`6` "
"keys, or the :kbd:`Ctrl + [` and :kbd:`Ctrl + ]` shortcuts for basic 15 "
"degrees rotation left and right. But you can also have more sophisticated "
"rotation with the :kbd:`Shift + Space + drag` or :kbd:`Shift +` |"
"mousemiddle| :kbd:`+ drag` shortcuts. To reset the rotation, press the :kbd:"
"`5` key."
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:34
msgid "Zoom"
msgstr "缩放"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:34
msgid ""
"You can use the :kbd:`+` and :kbd:`-` keys to zoom out and in, or use the :"
"kbd:`Ctrl +` |mousemiddle| shortcut. Use the :kbd:`1`, :kbd:`2` or :kbd:`3` "
"keys to reset the zoom, fit the zoom to page or fit the zoom to page width."
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:36
msgid ""
"You can use the Overview docker in :menuselection:`Settings --> Dockers` to "
"quickly navigate over your image."
msgstr ""
"你也可以使用总览图面板来快速控制图像视图，你可以在菜单栏的 :menuselection:`设"
"置 --> 工具面板` 处启用它。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:38
msgid ""
"You can also put these commands on the toolbar, so it'll feel a little like "
"Sai. Go to :menuselection:`Settings --> Configure Toolbars`. There are two "
"toolbars, but we'll add to the file toolbar."
msgstr ""
"你也可以把上述指令放到工具栏里，这样感觉就跟 SAI 比较像了。在菜单栏的 :"
"menuselection:`设置 --> 配置工具栏` 处打开对话框 。Krita 有两个工具栏，在这里"
"我们先假定你要添加按钮到文件/主工具栏 (file/mainToolbar) 。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:40
msgid ""
"Then, you can type in something in the left column to search for it. So, for "
"example, 'undo'. Then select the action 'undo freehand stroke' and drag it "
"to the right. Select the action to the right, and click :menuselection:"
"`Change text`. There, toggle :menuselection:`Hide text when toolbar shows "
"action alongside icon` to prevent the action from showing the text. Then "
"press :guilabel:`OK`. When done right, the :guilabel:`Undo` should now be "
"sandwiched between the save and the gradient icon."
msgstr ""
"在弹出的对话框中，在左栏的过滤框输入文字可搜索需要的按钮。例如输入“放大”，找"
"到放大按钮，然后把它拖放到右栏。如果界面有显示 :menuselection:`“更改文字”` 按"
"钮，则可以在那里勾选 :menuselection:`“当工具栏项目同时具有文字和图标时隐藏文"
"字”`，这样按钮将仅显示图标。完成后点击 :guilabel:`确定`，放大按钮就会被插入在"
"你刚才拖放它的位置了。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:42
msgid ""
"You can do the same for :guilabel:`Redo`, :guilabel:`Deselect`, :guilabel:"
"`Invert Selection`, :guilabel:`Zoom out`, :guilabel:`Zoom in`, :guilabel:"
"`Reset zoom`, :guilabel:`Rotate left`, :guilabel:`Rotate right`, :guilabel:"
"`Mirror view` and perhaps :guilabel:`Smoothing: basic` and :guilabel:"
"`Smoothing: stabilizer` to get nearly all the functionality of Sai's top bar "
"in Krita's top bar. (Though, on smaller screens this will cause all the "
"things in the brushes toolbar to hide inside a drop-down to the right, so "
"you need to experiment a little)."
msgstr ""
"按照上述办法可以把 :guilabel:`撤销`、:guilabel:`重做`、:guilabel:`取消选择"
"`、:guilabel:`反向选择`、:guilabel:`缩小`、:guilabel:`放大`、:guilabel:`重置"
"缩放`、:guilabel:`向左旋转`、:guilabel:`向右旋转`、:guilabel:`镜像显示`、:"
"guilabel:`笔刷平滑：基本`、:guilabel:`笔刷平滑：防抖` 等大部分 SAI 的工具栏功"
"能给添加到 Krita 的对应位置。如果屏幕太小，笔刷工具栏可能会被挤压成右边的一个"
"下拉菜单，所以你要进行一些实验。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:44
msgid ""
":guilabel:`Hide Selection`, :guilabel:`Reset Rotation` are currently not "
"available via the Toolbar configuration, you'll need to use the shortcuts :"
"kbd:`Ctrl + H` and :kbd:`5` to toggle these."
msgstr ""
":guilabel:`隐藏选区` 等一些功能目前在配置工具栏对话框中不可用。可按 :kbd:"
"`Ctrl + H` 进行切换。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:48
msgid ""
"Krita 3.0 currently doesn't allow changing the text in the toolbar, we're "
"working on it."
msgstr ""
"Krita 从 3.0 版开始无法更改工具栏文本标签的显示方式。我们正在处理这个问题。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:51
msgid "Right click color picker"
msgstr "右键拾色器"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:53
msgid ""
"You can actually set this in :menuselection:`Settings --> Configure Krita --"
"> Canvas input settings --> Alternate invocation`. Just double-click the "
"entry that says :kbd:`Ctrl +` |mouseleft| shortcut before :guilabel:`Pick "
"foreground color from image` to get a window to set it to |mouseright|."
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:57
msgid ""
"Krita 3.0 actually has a Paint-tool Sai-compatible input sheet shipped by "
"default. Combine these with the shortcut sheet for Paint tool Sai to get "
"most of the functionality on familiar hotkeys."
msgstr ""
"Krita 3.0 之后已经预装了一组兼容 Paint Tool SAI 的画布输入方案，配合同样是预"
"装的兼容 Paint Tool SAI 的快捷键方案使用，可以让 Krita 使用和 SAI 接近的快捷"
"键。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:60
msgid "Stabilizer"
msgstr "防抖"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:62
msgid ""
"This is in the tool options docker of the freehand brush. Use Basic "
"Smoothing for more advanced tablets, and Stabilizer is much like Paint Tool "
"Sai's. Just turn off :guilabel:`Delay` so that the dead-zone disappears."
msgstr ""
"在使用手绘笔刷工具时，防抖功能会被显示在工具选项面板。如果数位板性能较好，可"
"以选用“基本”笔刷平滑模式；选用“防抖”则可以得到类似 SAI 的效果。取消勾选 :"
"guilabel:`延迟` 选项可以关闭盲区功能。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:65
msgid "Transparency"
msgstr "透明区域"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:67
msgid ""
"So one of the things that throw a lot of Paint Tool Sai users off is that "
"Krita uses checkers to display transparency, which is actually not that "
"uncommon. Still, if you want to have the canvas background to be white, this "
"is possible. Just choose :guilabel:`Background: As Canvas Color` in the new "
"image dialogue and the image background will be white. You can turn it back "
"to transparent via :menuselection:`Image --> Change image background color`. "
"If you export a PNG or JPG, make sure to uncheck :guilabel:`Save "
"transparency` and to make the background color white (it's black by default)."
msgstr ""
"SAI 用户最不习惯的一个方面就是 Krita 用棋盘格来代表透明度，尽管这才是计算机图"
"形领域的通行做法。当然，你也可以把画布背景设成白色。在新建文件对话框的“内"
"容”页面选中 :guilabel:`背景：作为画布颜色`，这样图像的背景就会被固定为白色。"
"在创建文件之后，你还可以在菜单栏的 :menuselection:`图像 --> 图像背景色与透明"
"度` 更改背景色。在导出 PNG 时如果不想要透明背景色，可以取消勾选 :guilabel:`保"
"存透明度通道`，将背景色设为白色 (默认状态为黑色)。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:72
msgid ""
"Like Sai, you can quickly turn a black and white image to black and "
"transparent with the :menuselection:`Color to Alpha Filter` under :"
"menuselection:`Filters --> Colors --> Color to Alpha`."
msgstr ""
"和 SAI 一样，你可以把黑白图像的白色区域替换成透明区域。在菜单栏点击 :"
"menuselection:`滤镜 --> 颜色 --> 颜色转为透明度`。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:75
msgid "Brush Settings"
msgstr "笔刷选项"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:77
msgid ""
"Another, somewhat amusing misconception is that Krita's brush engine is not "
"very complex. After all, you can only change the Size, Flow and Opacity from "
"the top bar."
msgstr ""
"有些人会误以为 Krita 的笔刷引擎过于简陋——毕竟在顶部工具栏里你只能更改笔刷大"
"小、流量和不透明度。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:79
msgid ""
"This is not quite true. It's rather that we don't have our brush settings in "
"a docker but a drop-down on the toolbar. The easiest way to access this is "
"with the :kbd:`F5` key. As you can see, it's actually quite complex. We have "
"more than a dozen brush engines, which are a type of brush you can make. The "
"ones you are used to from Paint Tool Sai are the Pixel Brush (ink), The "
"Color Smudge Brush (brush) and the filter brush (dodge, burn)."
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:81
msgid ""
"A simple inking brush recipe for example is to take a pixel brush, uncheck "
"the :guilabel:`Enable Pen Settings` on opacity and flow, and uncheck "
"everything but size from the option list. Then, go into brush-tip, pick :ref:"
"`auto_brush_tip` from the tabs, and set the size to 25 (right-click a blue "
"bar if you want to input numbers), turn on anti-aliasing under the brush "
"icon, and set fade to 0.9. Then, as a final touch, set spacing to 'auto' and "
"the spacing number to 0.8."
msgstr ""
"以一个简单的描线笔刷为例，你可以选取一个像素笔刷，取消勾选“不透明度”和“流"
"量”的 :guilabel:`启用压感笔传感器选项`，并取消勾选列表中除了“大小”之外的所有"
"选项。接着来到笔尖页面，选中 :ref:`auto_brush_tip`，然后把直径设成 25 (要输入"
"具体数值，可以单击蓝色滑动条)，勾选笔刷轮廓下方的“抗锯齿”选项，将“淡化”设成 "
"0.9。最后将“间距”设成自动，并把数值设为 0.8。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:83
msgid ""
"You can configure the brushes in a lot of detail, and share the packs with "
"others. Importing of packs and brushes can be done via the :menuselection:"
"`Settings --> Manage Resources`, where you can import .bundle files or .kpp "
"files."
msgstr ""
"你可以对笔刷特性进行深入的配置，并且将成果与其他人分享。要导入笔刷文件或者资"
"源包，可在菜单栏的 :menuselection:`设置 --> 管理资源` 对话框导入 .bundle 或"
"者 .kpp 文件。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:86
msgid "Erasing"
msgstr "擦除"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:88
msgid ""
"Erasing is a blending mode in Krita, much like the transparency mode of "
"Paint Tool Sai. It's activated with the :kbd:`E` key or you can select it "
"from the Blending Mode drop-down..."
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:91
msgid "Blending Modes"
msgstr "混色模式"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:93
msgid ""
"Krita has a lot of Blending modes, and thankfully all of Paint Tool Sai's "
"are amongst them except binary. To manage the blending modes, each of them "
"has a little check-box that you can tick to add them to the favorites."
msgstr ""
"Krita 有许多混色模式，而且巧合的是除了二进制模式外，SAI 的每个模式都可以在 "
"Krita 里面找到对应的模式。要把混色模式添加到常用分类或者从中移除，可以勾选或"
"者取消勾选混色模式左边的复选框。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:95
msgid ""
"Multiple, Screen, Overlay and Normal are amongst the favorites. Krita's "
"Luminosity is actually slightly different from Paint Tool Sai's and it "
"replaces the relative brightness of color with the relative brightness of "
"the color of the layer."
msgstr ""
"相乘、滤色、叠加和正常混色模式都在常用分类中。Krita 的光度计算和 SAI 的略有不"
"同，它把颜色的相对亮度替换成了图层中颜色的相对亮度。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:98
msgid ""
"Sai's Luminosity mode is actually the same as Krita's *Addition* or *linear "
"dodge* mode. The Shade mode is the same as *Color Burn* and *Hard Mix* is "
"the same as the lumi and shade mode."
msgstr ""
"SAI 的“光度”模式和 Krita 的“相加”或者“线性减淡”模式相同。SAI 的“阴影”模式和 "
"Krita 的“颜色加深”模式相同。SAI 的“光度和阴影” 模式和Krita 的“硬混合”模式相"
"同。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:101
msgid "Layers"
msgstr "图层"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:103
msgid "Lock Alpha"
msgstr "锁定透明度"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:104
msgid "This is the checker box icon next to every layer."
msgstr ""
"Krita 的图层面板的每个图层右边都有一个 alpha 字母按钮，它就是“锁定透明度”功能"
"的开关。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:105
msgid "Clipping group"
msgstr "剪贴分组"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:106
msgid ""
"For Clipping masks in Krita you'll need to put all your images in a single "
"layer, and then press the 'a' icon, or press the :kbd:`Ctrl + Shift + G` "
"shortcut."
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:107
msgid "Ink layer"
msgstr "描线图层"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:108
msgid "This is a vector layer in Krita, and also holds the text."
msgstr "这是 Krita 里面的矢量图层，这种图层也被用来处理文字。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:109
msgid "Masks"
msgstr "蒙版"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:110
msgid ""
"These grayscale layers that allow you to affect the transparency are called "
"transparency masks in Krita, and like Paint Tool Sai, they can be applied to "
"groups as well as layers. If you have a selection and make a transparency "
"mask, it will use the selection as a base."
msgstr ""
"用来影响透明度的灰阶图层在 Krita 里面叫做透明度蒙版。和 SAI 里面一样，透明度"
"蒙版可以应用到单个图层或者图层分组。如果你当前有一个活动选区，在创建透明度蒙"
"版时将以该选区作为蒙版轮廓。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:112
msgid "Clearing a layer"
msgstr "清空图层"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:112
msgid ""
"This is under :menuselection:`Edit --> Clear`, but you can also just press "
"the :kbd:`Del` key."
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:115
msgid "Mixing between two colors"
msgstr "混合两种颜色"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:117
msgid ""
"If you liked this docker in Paint Tool Sai, Krita's Digital Color Selector "
"docker will be able to help you. Dragging the sliders will change how much "
"of a color is mixed in."
msgstr ""
"如果你喜欢 SAI 的这个工具面板，你也可以用 Krita 的“数字混色器”面板来实现类似"
"的功能。拖动数字混色器的滑动条就可以更改颜色的混合比例。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:120
msgid "What do you get extra when using Krita?"
msgstr "Krita 相对于 SAI 的优势"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:123
msgid "More brush customization"
msgstr "更强大的笔刷定制选项"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:125
msgid ""
"You already met the brush settings editor. Sketch brushes, grid brushes, "
"deform brushes, clone brushes, brushes that are textures, brushes that "
"respond to tilt, rotation, speed, brushes that draw hatches and brushes that "
"deform the colors. Krita's variety is quite big."
msgstr ""
"刚才我们已经介绍过笔刷选项编辑器，它的选项非常丰富。你可以在里面找到包括草"
"图、网格、扭曲、克隆等多种笔刷引擎，笔尖可以使用材质，笔迹可以感应笔压、倾"
"斜、旋转、速度等，有些笔刷可以画出排线，有些笔刷可以改变颜色。而这些仅仅是 "
"Krita 笔刷引擎功能的冰山一角。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:128
msgid "More color selectors"
msgstr "更多样的拾色器形式"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:130
msgid ""
"You can have HSV sliders, RGB sliders, triangle in a hue ring. But you can "
"also have HSI, HSL or HSY' sliders, CMYK sliders, palettes, round selectors, "
"square selectors, tiny selectors, big selectors, color history and shade "
"selectors. Just go into :menuselection:`Settings --> Configure Krita --> "
"Advanced Color Selector Settings` to change the shape and type of the main "
"big color selector."
msgstr ""
"除了常规的 HSV 滑动条、RGB 滑动条、色相环和它里面的明度、饱和度色三角外，你还"
"可以使用 HSI、HSL、HSY'、CMYK 等滑动条、各种调色板、圆形和方形的拾色器、大型"
"和小型的拾色器、颜色历史和光影拾色器等。你可以在菜单栏的 :menuselection:`设"
"置 --> 配置 Krita --> 拾色器设置` 里面配置高级拾色器。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:135
msgid ""
"You can call the color history with the :kbd:`H` key, common colors with "
"the :kbd:`U` key and the two shade selectors with the :kbd:`Shift + N` and :"
"kbd:`Shift + M` shortcuts. The big selector can be called with the :kbd:"
"`Shift + I` shortcut on canvas."
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:138
msgid "Geometric Tools"
msgstr "几何形状工具"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:140
msgid "Circles, rectangles, paths, Krita allows you to draw these easily."
msgstr "你可以在 Krita 里面轻而易举地画出圆形、矩形、路径等。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:143
msgid "Multibrush, Mirror Symmetry and Wrap Around"
msgstr "多重画笔、镜像对称和环绕模式"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:145
msgid ""
"These tools allow you to quickly paint a mirrored image, mandala or tiled "
"texture in no time. Useful for backgrounds and abstract vignettes."
msgstr ""
"这些工具可以帮助你快速绘制镜像图像、万花筒效果或者重复拼接的材质。对于绘制背"
"景和抽象插图很有用。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:151
msgid "Assistants"
msgstr "绘画辅助尺"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:153
msgid ""
"The painting assistants can help you to set up a perspective, or a "
"concentric circle and snap to them with the brush."
msgstr ""
"绘画辅助尺工具可以帮助你建立透视或者同心圆等形状，然后把笔刷吸附到它们上面。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:159
msgid ""
".. image:: images/assistants/Krita_basic_assistants.png\n"
"   :alt: Krita's vanishing point assistants in action"
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:159
msgid "Krita's vanishing point assistants in action"
msgstr "Krita 的消失点辅助尺正在工作"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:162
msgid "Locking the Layer"
msgstr "锁定图层"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:164
msgid "Lock the layer with the padlock so you don't draw on it."
msgstr "用图层面板里面的锁形图标可以锁定图层，避免在上面进行描绘。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:167
msgid "Quick Layer select"
msgstr "快速选择图层"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:169
msgid ""
"If you hold the :kbd:`R` key and press a spot on your drawing, Krita will "
"select the layer underneath the cursor. Really useful when dealing with a "
"large number of layers."
msgstr ""

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:172
msgid "Color Management"
msgstr "色彩管理"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:174
msgid ""
"This allows you to prepare your work for print, or to do tricks with the LUT "
"docker so you can diagnose your image better. For example, using the LUT "
"docker to turn the colors grayscale in a separate view, so you can see the "
"values instantly."
msgstr ""
"此功能可以帮助你进行印前准备，或者使用 LUT 面板来更好地分析图像。例如，你可以"
"为当前图像建立一个新视图，然后用 LUT 面板设定这个视图显示光度，这样即可实时观"
"察图像的明暗变化。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:180
msgid "Advanced Transform Tools"
msgstr "高级变形工具"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:182
msgid ""
"Not just rotate and scale, but also cage, wrap, liquify and non-destructive "
"transforms with the transform tool and masks."
msgstr ""
"在 Krita 里面你不仅可以进行旋转和缩放变形，还可以使用外框、网面、液化等高级变"
"形功能。你还可以使用变形蒙版进行非破坏性变形。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:188
msgid "More Filters and non-destructive filter layers and masks"
msgstr "更丰富的滤镜、非破坏性的滤镜和蒙版图层"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:190
msgid ""
"With filters like color balance and curves you can make easy shadow layers. "
"In fact, with the filter layers and layer masks you can make them apply on "
"the fly as you draw underneath."
msgstr ""
"你可以使用颜色平衡和曲线等滤镜图层来建立简单的阴影图层。滤镜图层可以让你一边"
"绘制一边自动把最新效果应用到它关联的图层上面。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:196
msgid "Pop-up palette"
msgstr "浮动画具板"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:198
msgid ""
"This is the little circular thing that is by default on the right click. You "
"can organize your brushes in tags, and use those tags to fill up the pop-up "
"palette. It also keeps a little color selector and color history, so you can "
"switch brushes on the fly."
msgstr ""
"默认状态下，在画布上右键单击会弹出这个圆形的浮动画具板。你可以给常用的笔刷预"
"设指定标签分组，然后把它们按组加载到这个面板上，随手切换非常方便。它上面还显"
"示了一个小型的拾色器和颜色历史。"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:204
msgid "What does Krita lack compared to Paint Tool Sai?"
msgstr "Krita 相对于 SAI 的不足"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:206
msgid "Variable width vector lines"
msgstr "矢量线条的宽度不便调整"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:207
msgid "The selection source option for layers"
msgstr "本地选区图层缺少来源选项"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:208
msgid "Dynamic hard-edges for strokes (the fringe effect)"
msgstr "动态笔刷硬边 (边缘效果)"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:209
msgid "No mix-docker"
msgstr "没有混色面板"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:210
msgid "No Preset-tied stabilizer"
msgstr "预设无法内置防抖选项"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:211
msgid "No per-preset hotkeys"
msgstr "不能为不同预设指定不同快捷键"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:214
msgid "Conclusion"
msgstr "结语"

#: ../../user_manual/introduction_from_other_software/introduction_from_sai.rst:216
msgid ""
"I hope this introduction got you a little more excited to use Krita, if not "
"feel a little more at home."
msgstr ""
"希望本文可以让你对 Krita 的特有功能有所期待，并在使用 Krita 时感到更加顺手。"
