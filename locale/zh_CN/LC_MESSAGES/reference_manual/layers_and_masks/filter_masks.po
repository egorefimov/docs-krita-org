msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___layers_and_masks___filter_masks.pot\n"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:None
msgid ".. image:: images/Krita_ghostlady_2.png"
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:None
msgid ".. image:: images/Krita_ghostlady_3.png"
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:1
msgid "How to use filter masks in Krita."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:11
msgid "Layers"
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:11
msgid "Masks"
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:11
msgid "Filters"
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:16
msgid "Filter Masks"
msgstr "滤镜蒙版"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:18
msgid ""
"Filter masks show an area of their layer with a filter (such as blur, "
"levels, brightness / contrast etc.). For example, if you select an area of a "
"paint layer and add a Filter Layer, you will be asked to choose a filter. If "
"you choose the blur filter, you will see the area you selected blurred."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:24
msgid ""
"With filter masks, we can for example make this ghost-lady more ethereal by "
"putting a clone layer underneath, and setting a lens-blur filter on it."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:30
msgid ""
"Set the blending mode of the clone layer to :guilabel:`Color Dodge` and she "
"becomes really spooky!"
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:32
msgid ""
"Unlike applying a filter to a section of a paint layer directly, filter "
"masks do not permanently alter the original image. This means you can tweak "
"the filter (or the area it applies to) at any time. Changes can always be "
"altered or removed."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:34
msgid ""
"Unlike filter layers, filter masks apply only to the area you have selected "
"(the mask)."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:36
msgid ""
"You can edit the settings for a filter mask at any time by double clicking "
"on it in the Layers docker. You can also change the selection that the "
"filter mask affects by selecting the filter mask in the Layers docker and "
"then using the paint tools in the main window. Painting white includes the "
"area, painting black excludes it, and all other colors are turned into a "
"shade of gray which applies proportionally."
msgstr ""
