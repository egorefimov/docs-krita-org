# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-08 17:34+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: icons image toolfreehandpath images alt ref\n"
"X-POFile-SpellExtra: freehandpathtool shapebrushengine\n"

#: ../../<rst_epilog>:36
msgid ""
".. image:: images/icons/freehand_path_tool.svg\n"
"   :alt: toolfreehandpath"
msgstr ""
".. image:: images/icons/freehand_path_tool.svg\n"
"   :alt: ferramenta de caminho livre"

#: ../../reference_manual/tools/freehand_path.rst:1
msgid "Krita's freehand path tool reference."
msgstr "A referência da ferramenta de desenho de caminhos à mão livre."

#: ../../reference_manual/tools/freehand_path.rst:10
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/freehand_path.rst:10
msgid "Vector"
msgstr "Vector"

#: ../../reference_manual/tools/freehand_path.rst:10
msgid "Freehand"
msgstr "Caminho Livre à Mão"

#: ../../reference_manual/tools/freehand_path.rst:15
msgid "Freehand Path Tool"
msgstr "Ferramenta de Caminho Livre à Mão"

#: ../../reference_manual/tools/freehand_path.rst:17
msgid "|toolfreehandpath|"
msgstr "|toolfreehandpath|"

#: ../../reference_manual/tools/freehand_path.rst:19
msgid ""
"With the Freehand Path Tool you can draw a path (much like the :ref:"
"`shape_brush_engine`) the shape will then be filled with the selected color "
"or pattern and outlined with a brush if so chosen. While drawing a preview "
"line is shown that can be modified in pattern, width and color."
msgstr ""
"Com a Ferramenta de Caminho Livre poderá desenhar um caminho (de forma "
"semelhante à :ref:`shape_brush_engine`), sendo que a forma depois será "
"preenchida com a cor ou padrão seleccionados e contornada com um dado "
"pincel, se for escolhida essa opção. Enquanto desenhar, irá aparecer uma "
"linha de antevisão que poderá ser modificada no padrão, espessura e cor."

#: ../../reference_manual/tools/freehand_path.rst:21
msgid ""
"This tool can be particularly good for laying in large swaths of color "
"quickly."
msgstr ""
"Esta ferramenta pode ser particularmente boa para dispor grandes blocos de "
"cor de forma rápida."
