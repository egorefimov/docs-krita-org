msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 03:07+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<generated>:1
msgid "Profile"
msgstr "Profil"

#: ../../reference_manual/preferences/canvas_input_settings.rst:1
msgid "Canvas input settings in Krita."
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
#: ../../reference_manual/preferences/canvas_input_settings.rst:16
msgid "Canvas Input Settings"
msgstr "Configuration des entrées"

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Tablet"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:18
msgid ""
"Krita has ways to set mouse and keyboard combinations for different actions. "
"The user can set which combinations to use for a certain Krita command over "
"here. This section is under development and will include more options in "
"future."
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:21
msgid "The user can make different profiles of combinations and save them."
msgstr ""
